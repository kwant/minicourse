---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.7.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Computational Quantum Transport with Kwant

<img src="./1_figures/kwant_gallery.png" alt="Drawing" style="width: 800px;"/>

In this first session, we will:
* Introduce a continuum scattering problem in one dimension
* Show how to get the corresponding discrete scattering problem
* Show how to solve it numerically with Python
* Show how to solve it better with Kwant
* And move on to more interesting 2D models
* Including graphene

There is much more information, including a full documentation of Kwant API and step by step tutorials available at https://kwant-project.org


## 1. A one dimensional scattering problem
A scattering problem contains:
* A finite scattering region
* Infinite leads that are invariant by translation

Example: a 1D problem:
\begin{equation}
  -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2} \Psi(x) + V(x) \Psi(x) = E \Psi(x)
\end{equation}
with
\begin{equation}
V(x) \ne  0 \quad \text{for} \quad  0 \le x \le L
\end{equation}


<img src="./1_figures/continuum.png" alt="Drawing" style="width: 400px;"/>


## 2. Discretizing the scattering problem.

(Pro tip: here we'll do it by hand but the kwant.discretizer module can do it for you)

\begin{equation}
\frac{\partial^2}{\partial x^2} \Psi(x) \approx \frac{1}{a^2}
[ \Psi(x-a) - 2\Psi(x) + \Psi(x+a) ]
\end{equation}
so if we define,
\begin{equation}
 \Psi_n = \Psi(x = na) \\
 V_n = V(x = na)  \\
 t = \frac{\hbar^2}{2ma^2} \\
\end{equation}
we get,
\begin{equation}
 - t \Psi_{n-1} + (V_n+2t) \Psi_n -t \Psi_{n+1} = E \Psi_n
\end{equation}

<img src="./1_figures/discrete.png" alt="Drawing" style="width: 600px;"/>

## 3. Setting up the linear problem
To simplify things a bit, we use $t$ as the unit of energy (i.e. set $t=1$)
and absorb the $2t$ into a redefinition of $V_n$:

\begin{equation}
-\Psi_{n+1}  -\Psi_{n-1} + V_n \Psi_{n} = E \Psi_{n}.
\end{equation}
In the electrodes, we have a superposition of plane waves
\begin{equation}
\text{For}\quad n\le 0 \quad \Psi_n = e^{ik n} +  r e^{-ik n}
\end{equation}
\begin{equation}
\text{For}\quad n > L \quad \Psi_n = t e^{ik n}
\end{equation}
with
\begin{equation}
E = -2\cos k
\end{equation}

Now finally, we need to "match" the wave function at the electrode-central region
interface:
\begin{eqnarray}
n=0   &:& r e^{-ik}  -\Psi_1 = -e^{ik } \\
n=1   &:& r + (E-V_1) \Psi_1 + \Psi_2 = -1 \\
n=p   &:& \Psi_{p-1} + (E-V_p) \Psi_{p} + \Psi_{p+1}  = 0 \\
n=L   &:& \Psi_{L-1} + (E-V_L)\Psi_{L} + t e^{ik(L+1)} = 0 \\
n=L+1 &:& \Psi_L - t e^{ikL} = 0
\end{eqnarray}
Which is simply a linear system of equations.
\begin{equation}
\left(\begin{matrix}
e^{-ik} & -1 & 0 & ... & ...& ... \\
1 & E-V_1 & 1 & 0 & ...& ...\\
0 &  1 & E-V_2 & 1 & 0 & ...\\
... & ... &... & ... & ... & ... \\
... &... & 0 & 1 & E-V_L &  e^{ik(L+1)} \\
... & ... & ... & 0 & 1  &-e^{ikL}
\end{matrix}\right)
\left(\begin{matrix}
r \\ \Psi_1 \\ \Psi_2 \\... \\ \Psi_L \\ t
\end{matrix}\right) =
\left(\begin{matrix}
-e^{ik } \\ -1 \\ 0 \\... \\ 0 \\ 0
\end{matrix}\right)
\end{equation}
that we can put in the computer!

<img src="./1_figures/continuum_right.png" alt="Drawing" style="width: 400px;"/>
The exercise can be repeated for electrons incoming from the right.
And we finally get the full "scattering matrix" $S$,
\begin{equation}
S = \left(
\begin{matrix} r & t \\ t' & r' \end{matrix}
\right)
\end{equation}

## 4. Calculating observables: the Landauer formula

### Particle current

* The particle current density associated with a  wave function reads,
\begin{equation}
I(x) = i\frac{\hbar}{2m} \left[\Psi (x)^* \frac{\partial}{\partial x} \Psi (x) -
\Psi (x) \frac{\partial}{\partial x} \Psi^* (x)\right]
\end{equation}
or in its discrete form,
\begin{equation}
I_n = i\frac{1}{\hbar} [\Psi_{n}^* \Psi_{n+1} - \Psi_{n+1}^* \Psi_{n}]
\end{equation}
For a plane wave,
\begin{equation}
\Psi_{n} = e^{ikn}
\end{equation}
we get
\begin{equation}
I_n = i \frac{1}{\hbar}[e^{ik} - e^{-ik}] = \frac{1}{\hbar} v_k
\end{equation}
\begin{equation}
v_k \equiv \frac{\partial}{\partial k} E(k)
\end{equation}

* For a scattering state,
\begin{equation}
\Psi_{n} = e^{ikn} + r e^{-ikn}
\end{equation}
\begin{equation}
I_n = v_k [ 1 - |r|^2 ]
\end{equation}

* Current conservation implies that $1 - |r|^2 = |t|^2 = |t'|^2 = 1 - |r'|^2$
and more generally that the S matrix is unitary,
\begin{equation}
S S^\dagger = 1
\end{equation}

### Statistical physics

Now to get the actual current, we need to fill up the states up to the Fermi level,
\begin{equation}
I = \frac{e}{\hbar} \int \frac{dk}{2\pi}
[ v_k [ 1 - |r|^2 ] f_L[E(k)] - v_k |t'|^2  f_R[E(k)] ]
\end{equation}
with
\begin{equation}
f_L(E)= \frac{1}{e^{(E-\mu_L)/kT_L} + 1}
f_R(E)= \frac{1}{e^{(E-\mu_R)/kT_R} + 1}
\end{equation}
In 1D, the velocity $dE/dk$ is the inverse of the density of state $dk/dE$, so that,
\begin{equation}
I = \frac{e}{h} \int dE  |t|^2  [f_L[E(k)] - f_R[E(k)] ]
\end{equation}
which at zero temperature and small bias $\mu_L = E_F + eV/2$,
$\mu_R = E_F - eV/2$ simplifies into the
celebrated Landauer formula,
\begin{equation}
I = \frac{e^2}{h} |t(E_F)|^2  V
\end{equation}

## 5. Time to see how to do these calculations with Kwant.

### Initialize a system with 20 sites

```python
from matplotlib import pyplot
import kwant

lat = kwant.lattice.square()     # A simple square lattice
syst = kwant.Builder()           # An empty system

syst[(lat(x, 0) for x in range(20))] = 0
kwant.plot(syst);
```

### Add hoppings between sites

```python
syst[((lat(x, 0), lat(x+1, 0)) for x in range(19))] = -1
kwant.plot(syst);
```

### Get the Hamiltonian matrix and use another package (numpy) to get the spectrum

```python
import numpy.linalg as npl
import numpy as np

fsyst = syst.finalized()
syst_matrix = fsyst.hamiltonian_submatrix()

evs = npl.eigvalsh(syst_matrix)                   # Numerical value
pyplot.plot(evs, 'o')
evs_th = -2*np.cos(np.pi * np.arange(1, 21) / 21)
pyplot.plot(evs_th, 'k')                          # Analytical result
pyplot.xlabel("$n$")
pyplot.xticks([0, 5, 10, 15])                     # Integer ticks
pyplot.ylabel("$E_n$");
```

### Now let's make some leads

```python
sym = kwant.TranslationalSymmetry((-1, 0))  # The lead will be invariant by translation
lead_1 = kwant.Builder(sym)
lead_1[lat(0, 0)] = 0
lead_1[lat(0, 0), lat(1, 0)] = -1

kwant.plot(lead_1);
```

### It's always a good idea to check the lead's bands structure

```python
kwant.plotter.bands(lead_1.finalized(), show=False)  # From Kwant
ks = np.linspace(-np.pi, np.pi, 100)
pyplot.plot(ks, -2 * np.cos(ks), 'k--');             # From the analytical formula above
```

### OK, now we're ready to attach the leads to the central system and calculate the conductance

```python
syst.attach_lead(lead_1)
syst.attach_lead(lead_1.reversed())
kwant.plot(syst);
```

```python
fsyst = syst.finalized()
Es = np.linspace(-3, 3, 20)
gs = []
for E in Es:
    S = kwant.smatrix(fsyst, E)
    gs.append(S.transmission(1, 0))
pyplot.plot(Es, gs, 'ok-')
pyplot.ylabel("$g \quad [e^2/h]$")
pyplot.xlabel("$E_F \quad [t] $");
```

OK, that was a bit boring.
### Let's transform our 1d system into a Fabry-Perot resonator

<img src="./1_figures/FB_cavity.png" alt="Drawing" style="width: 400px;"/>


```python
#H_1 [(lat(x, 0) for x in range(20))] = 0  # Reset the potential
syst[lat(2, 0)] = 1             # Introduce a barrier on the left
syst[lat(14, 0)] = 1            # And a second one on the right

fsyst = syst.finalized()
Es = np.linspace(-3, 3, 500)
gs = []
for E in Es:
    S = kwant.smatrix(fsyst, E)
    gs.append(S.transmission(1, 0))
pyplot.plot(Es, gs, 'k-')
pyplot.xlabel("$E_F \quad [t] $")
pyplot.ylabel("$g \quad [e^2/h]$");
```

## 6 Variant of the Fabry-Perot example: Models in 1-d
### Let's review the previous example
(Complete except for imports and lattice definition)

```python
# Create central region.
syst = kwant.Builder()
syst[(lat(x, 0) for x in range(20))] = 0
syst[((lat(x, 0), lat(x+1, 0)) for x in range(19))] = -1

# Fabry-Perot
syst[lat(2, 0)] = 1
syst[lat(14, 0)] = 1

# Create and attach leads, finalize.
sym = kwant.TranslationalSymmetry((-1, 0))
lead = kwant.Builder(sym)
lead[lat(0, 0)] = 0
lead[lat(0, 0), lat(1, 0)] = -1
syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

# Finalize and perform computation.
fsyst = syst.finalized()
Es = np.linspace(-3, 3, 500)
gs = [kwant.smatrix(fsyst, E).transmission(1, 0) for E in Es]
pyplot.plot(Es, gs, 'k-');
```

Observe that we implement the same simple "model" twice: one time in the central region, one time in the lead.  This is no problem in this very simple example, but what if the model was more complicated?  Isn't there a way to reduce the redundancy?

Yes, we can define a high-symmetry model and use it as a template to **fill** the central region and the leads.

```python
# Create model.
sym = kwant.TranslationalSymmetry((-1, 0))
model = kwant.Builder(sym)
model[lat(0, 0)] = 0
model[lat(0, 0), lat(1, 0)] = -1

#### FILL central region with model. ####
syst = kwant.Builder()
syst.fill(model, lambda site: 0 <= site.tag[0] < 20, lat(0, 0))

# Just like before, we can modify the central region.
syst[lat(2, 0)] = 1
syst[lat(14, 0)] = 1

# We could create an empty lead and fill it with the model,
# but in 1d this would amount to copying the model.
# Instead: use it directly as lead.
syst.attach_lead(model)
syst.attach_lead(model.reversed())

# Finalize and perform computation.
fsyst = syst.finalized()
Es = np.linspace(-3, 3, 500)
gs = [kwant.smatrix(fsyst, E).transmission(1, 0) for E in Es]
pyplot.plot(Es, gs, 'k-');
```

## 7. More advanced systems: Quantum point contact

```python
from math import exp

# Model parameters
a = 0.5                     # lattice constant
m = 1.23                    # effective mass

# Create a square lattice with specified lattice constant.
# Specify that it has a single orbital per site.
sq = kwant.lattice.square(a=a, norbs=1)

# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite                     # Value function!
model[sq.neighbors()] = -1 / (2 * m * a**2)  # Constant value!

# We could have also assigned the hoppings individually:
# > model[sq(0, 0), sq(1, 0)] = model[sq(0, 0), sq(0, 1)] = ...
#
# Or a list of hoppings:
# > model[[(sq(0, 0), sq(1, 0)), (sq(0, 0), sq(0, 1))]] = ...

kwant.plot(model);
```

```python
# Geometric parameters
W = 9                       # width of central region
L = 14                      # length of central region

# Define shape of central region: a rectangle of length L and width W, centered
# around the origin.
def rectangle(site):
    x, y = site.pos
    return -L <= 2*x <= L and -W <= 2*y <= W

# Fill central region with model.
syst = kwant.Builder()
syst.fill(model, rectangle, sq(0, 0))

# Fill lead with same model, but with substituted parameter so that the
# potential strength in the leads can be set to zero.
lead = kwant.Builder(kwant.TranslationalSymmetry(sq.vec((-1, 0))))
lead.fill(model.substituted(voltage='voltage_lead'),
          lambda site: -W <= 2*site.pos[1] <= W,
          sq(0, 0))
syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

# Finalize the system and plot it, coloring the sites according to their potential
# (=their onsite hamiltonian value).
fsyst = syst.finalized()
params = dict(voltage_lead=0, voltage=-4, width=3, thickness=4)
kwant.plot(fsyst, site_color=lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
# The potential can be seen clearer when we use a different plotting function.
kwant.plotter.map(fsyst, lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
# Choose energy such that a few modes are open.
kwant.plotter.bands(fsyst.leads[0], params=params)
energy = -5
```

```python
# Plot conductance as a function of potential strength:
# We observe conductance quantization.
voltages = np.linspace(0, -4, 100)
gs = [kwant.smatrix(fsyst, energy, params=params).transmission(1, 0)
      for params['voltage'] in voltages]
pyplot.plot(voltages, gs, 'k-')
pyplot.xlabel("$voltage [t] $")
pyplot.ylabel("$g \quad [e^2/h]$");
```

## 8. Teaser: Graphene Aharonov-Bohm interferometer

### Let's build an Aharonov-Bohm loop

```python
lat = kwant.lattice.square()           # A simple square lattice
syst = kwant.Builder()                 # An empty system
r1, r2 = 10, 20

def ring(pos):
        (x, y) = pos
        rr = x**2 + y**2
        return (r1**2 < rr < r2**2)

# The magic shape() method allows one to easily build
# a device with the geometry you want
syst[lat.shape(ring, (0, r1 + 1))] = 0
syst[lat.neighbors()] = -1

kwant.plot(syst);
```

### We could make it from a different material, e.g. graphene

```python
lat = kwant.lattice.honeycomb()        # graphene lattice
# Or build it manually:
# lat = kwant.lattice.general([(1, 0), (sin_30, cos_30)],
#                             [(0, 0), (0, 1 / sqrt(3))])
# a, b = lat.sublattices

syst = kwant.Builder()                 # An empty system
r1, r2 = 5, 10

def ring(pos):
        (x, y) = pos
        rr = x**2 + y**2
        return (r1**2 < rr < r2**2)

syst[lat.shape(ring, (0, r1 + 1))] = 0
syst[lat.neighbors()] = -1

syst.eradicate_dangling()              # Quite convenient when using lat.shape()

kwant.plot(syst);
```

### Or even make a full four terminals device

```python
lat = kwant.lattice.honeycomb()        # graphene lattice
syst = kwant.Builder()                 # An empty system
r1, r2 = 5, 10

def TIE_fighter(pos):
    (x, y) = pos
    rr = x**2 + y**2
    ring = (r1**2 < rr < r2**2)
    wing = (x<r1//2 and x>-r1//2 and (abs(y-r2) <r1 or abs(y+r2) <r1))
    upper = (x<r2 and x>-r2 and y > r2+r1 and y <r2+2*r1)
    lower = (x<r2 and x>-r2 and y < -r2-r1 and y>-r2-2*r1)
    return ring or wing or upper or lower

syst[lat.shape(TIE_fighter, (0, r1 + 1))] = 0
syst[lat.neighbors()] = -1

syst.eradicate_dangling()               # Quite convenient when using lat.shape()

sym = kwant.TranslationalSymmetry((-1, 0))  # The lead will be invariant by translation

# TODO : add leads
lead_upper = kwant.Builder(sym)
def upper(pos):
    (x, y) = pos
    return x<20 and x>-20 and y > r2+r1 and y <r2+2*r1

# Beware the second argument of lat.shape is in real space
lead_upper[lat.shape(upper, lat.a(-8, 17).pos)] = 0

lead_upper[lat.neighbors()] = -1
syst.attach_lead(lead_upper)
syst.attach_lead(lead_upper.reversed())

lead_lower = kwant.Builder(sym)
def lower(pos):
    (x, y) = pos
    return x<20 and x>-20 and y < -r2-r1 and y>-r2-2*r1

# Beware the second argument of lat.shape is in real space
lead_lower[lat.shape(lower, lat.a(-8, -19).pos)] = 0
lead_lower[lat.neighbors()] = -1
lead_lower.eradicate_dangling()
syst.attach_lead(lead_lower)
syst.attach_lead(lead_lower.reversed())

kwant.plot(syst);
```

### The last thing we need is to add a magnetic field.
For that we will use two new pieces of the API: HoppingKind and
value functions.

```python
lat = kwant.lattice.honeycomb()        # graphene lattice
syst = kwant.Builder()                 # An empty system
r1, r2 = 5, 10

def hopx(site1, site2, B):
        # The magnetic field is controlled by the parameter B
        y = site1.pos[1]
        return - np.exp(-1j * B * y)

def TIE_fighter(pos):
        (x, y) = pos
        rr = x**2 / 8 + y**2
        ring = (r1**2 < rr < r2**2)
        wing = x<r1//2 and x>-r1//2 and (abs(y-r2) <r1 or abs(y+r2) <r1)
        upper = x<r2 and x>-r2 and y > r2+r1 and y <r2+2*r1
        lower = x<r2 and x>-r2 and y < -r2-r1 and y>-r2-2*r1
        return  ring or wing or upper or lower

syst[lat.shape(TIE_fighter, (0, r1 + 1))] = 0
syst[lat.neighbors()] = -1

#########################################################
# New magic line:
syst[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
#########################################################

syst.eradicate_dangling()               # Quite convenient when using lat.shape()

sym = kwant.TranslationalSymmetry((-1, 0))  # The lead will be invariant by translation

# TODO : add leads
lead_upper = kwant.Builder(sym)
def upper(pos):
    (x, y) = pos
    return x<20 and x>-20 and y > r2-1 and y <r2+2*r1

# Beware the second argument of lat.shape is in real space
lead_upper[lat.shape(upper, lat.a(-8, 17).pos)] = 0

lead_upper[lat.neighbors()] = -1
lead_upper[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
syst.attach_lead(lead_upper)
syst.attach_lead(lead_upper.reversed())

lead_lower = kwant.Builder(sym)
def lower(pos):
    (x, y) = pos
    return x<20 and x>-20 and y < -r2+1 and y>-r2-2*r1

# Beware the second argument of lat.shape is in real space
lead_lower[lat.shape(lower, lat.a(-8, -19).pos)] = 0
lead_lower[lat.neighbors()] = -1
lead_lower[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
lead_lower.eradicate_dangling()
syst.attach_lead(lead_lower)
syst.attach_lead(lead_lower.reversed())
```

```python
fsyst = syst.finalized()
EF = 0.5
Bs = np.linspace(0.06, 0.1, 200)
gs = []
for B in Bs:
    S = kwant.smatrix(fsyst, EF, params=dict(B=B))
    gs.append(S.transmission(3, 0))
pyplot.plot(Bs, gs, 'k-')
pyplot.ylabel("$dI_3/dV_0 \quad [e^2/h]$")
pyplot.xlabel("$B$");
```

We remind us of early mesoscopic experiments


<img src="./1_figures/webb.png" alt="Drawing" style="width: 400px;"/>

```python

```
