---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.7.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Simulating time-dependent transport with Tkwant


Tkwant: Non-stationary and manybody generalization of Kwant.

An introduction to Tkwant and details can be found at:
https://arxiv.org/abs/2009.03132


## I - Introduction


### Typical system


<img src="./images/scattering_region.png" alt="Drawing" style="width: 500px;"/>


### Hamiltonian: Tight-binding + perturbation



\begin{equation}
  \hat{H}(t) = \sum_{ij} H_{ij}(t) \hat{c}^\dagger_i \hat{c}_j
\end{equation}

\begin{equation}
    H(t) = H_0 + \Theta(t) W(t)
\end{equation}



### Observables: manybody expectation values  $\qquad \langle c^\dagger_ic_j\rangle(t) = {\rm Tr } [c^\dagger_ic_j \hat{\rho}(t) ]$



densities:
$n_i (t) = \langle c^\dagger_ic_i\rangle(t) $

currents:
$I(t) = i [ \langle \hat{c}^\dagger_{i_0}\hat{c}_{i_0+1} \rangle (t) - \langle \hat{c}^\dagger_{i_0+1} \hat{c}_{i_0} \rangle (t) ]$



### Example: Snapshots of the electron density $n(t)$ in a graphene dot after a pulse


<img src="./images/graphene.png" alt="Drawing" style="width: 1000px;"/>
Code see Ref. [2]


### Tkwant gallery


<img src="./images/tkwant_gallery.png" alt="Drawing" style="width: 800px;"/> 


- Mach-Zehnder and Fabry-Perot electronic interferometers
- Floquet topological insulator
- Quantum Hall regime
- Multiple Andreev reflections
- Skyrmion dynamics
- Graphene relaxation dynamics
- Quantum noise
- Heat transport and thermoelectic effect
- Plasmons and Luttinger liquids


## II - Wavefunction formalism (equivalent to Keldysh)


<img src="./images/tkwant_formalism.png" alt="Drawing" style="width: 1000px;"/>


## III - Example: Fabry-Perot interferometer


Sketch of the system
<img src="./images/fabry_perot_system.png" alt="Drawing" style="width: 600px;"/>

Hamiltonian
\begin{equation}
\label{eq:example_hamiltonian}
\hat{\mathbf{H}}(t) =  \sum_{i}^{N_s+1} \epsilon_i \hat{c}^\dagger_i \hat{c}_i - \sum_{-\infty}^{\infty} \hat{c}^\dagger_{i+1} \hat{c}_i -  [e^{i \phi(t)}-1]  \hat{c}^\dagger_{1} \hat{c}_{0} + \text{h.c.},
\end{equation}

\begin{equation}
\label{eq:phi}
\phi(t) = \frac{e}{\hbar} \int_{- \infty}^{t}  V(\tau) \, d\tau.
\end{equation}

```python
import tkwant
import kwant

from math import sin, pi
import matplotlib.pyplot as plt
```

```python
def make_fabry_perot_system():

    # Define an empty tight-binding system on a square lattice.
    lat = kwant.lattice.square(norbs=1)
    syst = kwant.Builder()

    # Central scattering region.
    syst[(lat(x, 0) for x in range(80))] = 0
    syst[lat.neighbors()] = -1
    # Backgate potential.
    syst[(lat(x, 0) for x in range(5, 75))] = -0.0956
    # Barrier potential.
    syst[[lat(4, 0), lat(75, 0)]] = 5.19615

    # Attach lead on the left- and on the right-hand side.
    sym = kwant.TranslationalSymmetry((-1, 0))
    lead = kwant.Builder(sym)
    lead[(lat(0, 0))] = 0
    lead[lat.neighbors()] = -1
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst, lat


# Phase from the time integrated voltage V(t).
def phi(time):
    vb, tau = 0.6, 30.
    if time > tau:
        return vb * (time - tau / 2.)
    return vb / 2. * (time - tau / pi * sin(pi * time / tau))
```

```python
# Make the system and add voltage V(t) to the left lead (index 0).
syst, lat = make_fabry_perot_system()
tkwant.leads.add_voltage(syst, 0, phi)
syst = syst.finalized()
```

```python
# Plot system
kwant.plot(syst);
```

```python
# Lead spectrum
chemical_potential = -1
kwant.plotter.bands(syst.leads[0], show=False)
plt.plot([-pi, pi], [chemical_potential] * 2, 'k--')
plt.show()
```

```python
# Time evaluation points
times = range(220)

# Define an operator to measure the current after the barrier.
hoppings = [(lat(78, 0), lat(77, 0))]
current_operator = kwant.operator.Current(syst, where=hoppings)

# Set occupation T = 0 and mu = -1 for both leads.
occupations = tkwant.manybody.lead_occupation(chemical_potential=-1)
```

**NOTE:** Running the simulation takes quite some time; 48 cores around 30 minutes compute time.

```python
# Initialize the time-dependent manybody state.
state = tkwant.manybody.State(syst, max(times), occupations)

# Loop over timesteps and evaluate the current.
currents = []
for time in times:
    state.evolve(time)
    current = state.evaluate(current_operator)
    currents.append(current)
```

```python
# Plot the result
plt.plot(times, currents / currents[-1])
plt.xlabel(r'time $t$')
plt.ylabel(r'current $I$')
plt.show()
```

<img src="./images/fabry_perot_result.png" alt="Drawing" style="width: 600px;"/> 


The steps in the $I(t)$ curve come from back and forth reflections in the Fabry-Perot cavity.
(first step: direct transmission, second step: one back-and-forth reflection, third step: two back back-and-forth reflections..).
Tiny oscillations on each step stem from the interference between the foreward- and counter-propagating waves.
Example from Ref: Gaury, Weston, Waintal *Nat. Commun.*, 6524, 2015. Code from Ref. [2].


## IV - Remarks

- Calculations much more heavy than Kwant


- Typical simulations on clusters on several cores in parallel (Message Passing Interface, MPI)


- Automatic and adaptive numerial routines (high-level approach)


- Flexible manual approach for customization (low-level approach)


### Parallelization with MPI
Speed up the computation by using several cores in parallel.

```python
comm = tkwant.mpi.get_communicator()
def am_master():
    return comm.rank == 0

for time in times:
    state.evolve(time)
    current = state.evaluate(current_operator)
    if am_master():
        currents.append(current)
```

on the command line:

$ mpirun -n 8 python3 my_tkwant_scrip.py


### Temperature and chemical potential in the leads

Have different chemical potentials and temperatures in the left and right lead.

```python
occup_left = tkwant.manybody.lead_occupation(chemical_potential=-1, temperature=0.2)
occup_right = tkwant.manybody.lead_occupation(chemical_potential=0.4, temperature=0.1)

state = tkwant.manybody.State(syst, max(times), occupations=[occup_left, occup_right])
```

### Numerical accuracy

Refine the quadrature to increase the numerical accuracy of the manybody integral.

```python
for time in times:
    state.evolve(time)
    
    error = state.estimate_error()
    print('time={}, error={}:'.format(time, error))
    
    state.refine_intervals(rtol=1E-8, atol=1E-8)
    
    current = state.evaluate(current_operator)
```

## V - References

[1] Website: 
https://tkwant.kwant-project.org/

[2] Article:
https://arxiv.org/abs/2009.03132



# Advanced Exercise: Simulating time-dependent transport with Tkwant


We consider an infinite one-dimensional chain described by the tight-binding Hamiltonian

\begin{equation}
\hat{\mathbf{H}}(t) =  \sum_{i=0}^{N-1} \epsilon(t, i) \hat{c}^\dagger_i \hat{c}_i - \sum_{-\infty}^{\infty} \hat{c}^\dagger_{i+1} \hat{c}_i + \text{h.c.}.
\end{equation}

Here, $\hat{c}^\dagger_i$ (respectively $\hat{c}_i$) is a fermionic creation (respectively annihilation)
operator on lattice site $i$. In the central scattering region ($i \in [0, N-1]$), an additional time and
space dependent onsite potential $\epsilon(t, i)$ is present. The potential is of gaussian shape in both temporal and spacial direction

\begin{equation}
\epsilon(t, i) = 1 + a e^{- ((t - t_0)/\tau)^2 - ((i - i_0)/\sigma)^2},
\end{equation}

and $a, t_0, i_0, \tau$ and $\sigma$ are numerical parameters.
Far away from the scattering region in both semi-infinite leads, the electrons are expected to be non-interacting and at thermal equilibrium with a chemical potential of $\mu = 0$ and at zero temperature.

We like to calculate the time evolution of the electron density
\begin{equation}
n_i (t) = \langle c^\dagger_ic_i\rangle(t) = {\rm Tr } [\hat{\rho}(t) c^\dagger_i c_i ]
\end{equation}
where $\hat{\rho}(t)$ is the non-equilibrium density matrix in the central scattering region.

The Kwant code to build the system is given already:

```python
import tkwant
import kwant

import numpy as np
import matplotlib.pyplot as plt


def gauss_function(x, x0, sigma):
    return np.exp(-((x - x0)/sigma)**2)


def make_system(L=100):

    def onsite_potential(site, time):
        t0 = 10         # center of the pulse in time
        i = site.pos[0] # spacial grid position
        i0 = L // 2     # center of the pulse in space
        sigma = 5       # width of the pulse
        return 1 + 0.1 * gauss_function(time, t0, sigma) * gauss_function(i, i0, sigma)

    # system building
    lat = kwant.lattice.square(a=1, norbs=1)
    syst = kwant.Builder()

    # central scattering region
    syst[(lat(x, 0) for x in range(L))] = onsite_potential
    syst[lat.neighbors()] = -1

    # add leads
    sym = kwant.TranslationalSymmetry((-1, 0))
    lead_left = kwant.Builder(sym)
    lead_left[lat(0, 0)] = 1
    lead_left[lat.neighbors()] = -1
    syst.attach_lead(lead_left)
    syst.attach_lead(lead_left.reversed())

    return syst


syst = make_system().finalized()
sites = np.array([site.pos[0] for site in syst.sites])
times = [10, 20, 30]
```

## Exercises

- 1) Plot the system as well as the shape of the potential in time and space. (No Tkwant needed)
- 2) Plot the energy dispersion of the leads in the first Brillouin zone. Make sure that you will find occupied states below the Fermi energy. (No Tkwant needed)
- 3) Simulate the density $n_i(t)$ for the three time snapshots in *times* with Tkwant. For presentation purpose, substract the equilibrium density at the initial time.
- 4) Same as above but perform an additional refinement of the manybody integral before evaluating the density. Print an estimate of the numerical error at each timestep.
- 5) Re-run above Tkwant simulation with logging enabled.

Note: The numerical Tkwant simulation in 3), 4) and 5) take roughly 1 minute on one core of a standard computer.


## Hints
- 3) https://kwant-project.org/extensions/tkwant/tutorial/getting_started
- 4) see the *High-level automatic approach* in https://kwant-project.org/extensions/tkwant/tutorial/manybody
- 5) https://kwant-project.org/extensions/tkwant/tutorial/logging **The notebook kernel must be restarted for this exercise.**



## Solutions


### 1) Plot the system and the potential shape in time and space

```python
kwant.plot(syst);  # plot of the central scattering system
```

```python
# plot of the onsite potential
timegrid = np.linspace(0, max(times))
plt.plot(timegrid, gauss_function(timegrid, 10, 5))
plt.xlabel(r'time $t$')
plt.ylabel(r'$\epsilon(t, i_0)$ (normalized to one)')
plt.show()

plt.plot(sites, gauss_function(sites, 50, 5))
plt.xlabel(r'sites $i$')
plt.ylabel(r'$\epsilon(t_0, i)$ (normalized to one)')
plt.show()
```

Note that the potential varies smoothly on time and space.


### 2) Plot the lead dispersion of the system

```python
# here we show the solution with the kwantspectrum module, using kwant.plotter.bands() is also possible

import kwantspectrum as ks
spec = ks.spectrum(syst.leads[0])

momenta = np.linspace(-np.pi, np.pi, 500)
for band in range(spec.nbands):
    plt.plot(momenta, spec(momenta, band), label='n=' + str(band))
plt.xlabel(r'$k$')
plt.ylabel(r'$E_n(k)$')
plt.legend()
plt.show()
```

The Fermi energy $E_F = 0$, so there are occupied states below this energy.


### 3) Time evolution of the system

```python
# use the density operator from kwant
density_operator = kwant.operator.Density(syst)
```

```python
# the actual tkwant simulation
state = tkwant.manybody.State(syst, max(times))

density0 = state.evaluate(density_operator)

for time in times:
    state.evolve(time=time)
    density = state.evaluate(density_operator)
    plt.plot(sites, density - density0, label='time={}'.format(time))

plt.legend()
plt.xlabel(r'site position $i$')
plt.ylabel(r'charge density $n$')
plt.show()
```

The perturbation is created at the center of the system and propagates to the left and the right side. Note that the waves are not reflected on both sides as the system is formally infinitely long (also called open system), but just continue its propagation outside entral region shown here. At long times, the system will relaxate to the initial equilibrium situation.
The propagation speed of the waves created by the perturbation is given by the group velocity $v = d E(k) / d k$ at the Fermi energy $E_F$.


### 4) Time evolution of the system with refinement and error estimate.

```python
state = tkwant.manybody.State(syst, max(times))

density0 = state.evaluate(density_operator)

for time in times:
    state.evolve(time=time)
    state.refine_intervals()
    error = state.estimate_error()
    print('time={}, error={:10.4e}'.format(time, error))
    density = state.evaluate(density_operator)
    plt.plot(sites, density - density0, label='time={}'.format(time))

plt.legend()
plt.xlabel(r'site position $i$')
plt.ylabel(r'charge density $n$')
plt.show()
```

Calling the method ``manybody.State.refine_intervals()`` refines the numerical quadrature for the manybody integral and guarantees that a certain accuracy is met (without arguments some default accuracy value).
One can estimate the error of an observable by calling ``manybody.State.estimate_error()``.

In this example the system is sufficiently simple and the perturbation smooth, such that the numerical quadrature at the initial time $t = 0$ is still a good approximation for the later timesteps. For this reason we can visually see no difference to the plot from the previous simulation from exercise 3) without numerical refinement. For more complicated systems and quantitative studies, numerical accuracy plays an important role however.


### 5) Tkwant simulation with logging.

**Restart the Notebook kernel to run this exercise**

```python
import tkwant
import kwant

import numpy as np
import matplotlib.pyplot as plt

#-------------------- enable logging --------------------------------
import logging

tkwant.logging.handler = tkwant.logging.simple_handler
tkwant.logging.level = logging.INFO
#--------------------------------------------------------------------



def gauss_function(x, x0, sigma):
    return np.exp(-((x - x0)/sigma)**2)


def make_system(L=100):

    def onsite_potential(site, time):
        t0 = 10         # center of the pulse in time
        i = site.pos[0] # spacial grid position
        i0 = L // 2     # center of the pulse in space
        sigma = 5       # width of the pulse
        return 1 + 0.1 * gauss_function(time, t0, sigma) * gauss_function(i, i0, sigma)

    # system building
    lat = kwant.lattice.square(a=1, norbs=1)
    syst = kwant.Builder()

    # central scattering region
    syst[(lat(x, 0) for x in range(L))] = onsite_potential
    syst[lat.neighbors()] = -1

    # add leads
    sym = kwant.TranslationalSymmetry((-1, 0))
    lead_left = kwant.Builder(sym)
    lead_left[lat(0, 0)] = 1
    lead_left[lat.neighbors()] = -1
    syst.attach_lead(lead_left)
    syst.attach_lead(lead_left.reversed())

    return syst


syst = make_system().finalized()
sites = np.array([site.pos[0] for site in syst.sites])
times = [10, 20, 30]

density_operator = kwant.operator.Density(syst)

state = tkwant.manybody.State(syst, max(times))

density0 = state.evaluate(density_operator)

for time in times:
    state.evolve(time=time)
    state.refine_intervals()
    error = state.estimate_error()
    print('time={}, error={:10.4e}'.format(time, error))
    density = state.evaluate(density_operator)
    plt.plot(sites, density - density0, label='time={}'.format(time))

plt.legend()
plt.xlabel(r'site position $i$')
plt.ylabel(r'charge density $n$')
plt.show()
```

The log output prints all numerical parameters of the adaptive routines and the processing steps of the Tkwant algorithm. Even without detailed knowledge of Tkwant, the output could be helpful to check for possible (user) errors and to get hints if anything goes wrong.

