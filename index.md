---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.7.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Quantum transport minicourse

Welcome!

This is a workshop organized by the [Kwant](https://kwant-project.org) team with help of many volunteers.

It is hosted by the [Virtual Science Forum](https://virtualscienceforum.org), and is using the [binder](https://mybinder.org) cloud computational environment.

We hope that the workshop is enjoyable and useful for you. That said, its format is *highly* experimental, so please be patient if something doesn't work or if the material level is too high or too low. We would love to hear your feedback, and will send a survey after the workshop.

Also don't worry if you don't catch something: all the materials will stay available, and you will be able to study on your own later. If you have follow-up questions, you can also reach us later via the [Kwant mailing list](https://mail.python.org/archives/list/kwant-discuss@python.org/).

## Organization

- Use chat to ask questions during the lecture (the schedule is tight)
- If you have a question during the exercise sessions, find an *instructor* breakout room with the fewest participants and join it.
- If the instructor is occupied, use "raise hand" feature.
- Share your screen to show your working.
- Feel free to join the other breakout rooms for group work if you want to work together on something, but be considerate: the number of breakout rooms is limited.
- Binder has a strict timeout (your server will be shut down after long inactivity). If that happens use "save to browser" button. You can also run the code below to prevent the kernel from shutting down.

```python
from time import sleep

sleep(30 * 60)
```

## Contents

1. [Introduction](1_introduction.md)
2. [Basic exercises](2_basic_exercises.md)
3. [Advanced concepts](3_advanced_concepts.md)
4. [Advanced exercises](4_advanced_exercises.md)
5. [Time-dependent transport](5_time_dependent_transport.md)
6. [Symmetry analysis](6_symmetry_analysis.md)
7. [KPM](7_kpm.md)
8. [Adaptive](8_adaptive.md)

Run these interactively [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kwant-project.org%2Fkwant%2Fminicourse.git/master?filepath=index.md) or download a [zip archive](https://gitlab.kwant-project.org/kwant/minicourse/builds/artifacts/master/raw/qt_workshop_notebooks.zip?job=prepare%20archive).
