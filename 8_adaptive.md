# Advanced topic: Adaptive

## The problem

In our research we often construct some mathematical model, implement them in code, and explore different parameter spaces to learn about the model's behaviour.

When sweeping a parameter space, we most often (because it is the easiest thing to do) sample a homogeneous space, like:


```python
import matplotlib.pyplot as plt
import numpy as np

def complicated_model(x, offset=0.1234, wait=True):
    from time import sleep
    from random import random

    a = 0.01
    if wait:
        sleep(random() / 5)  # we sleep to pretend like this is a slow function
    return x + a**2 / (a**2 + (x - offset)**2)


def sweep(n):
    xs = np.linspace(-1, 1, n)

    ys = []
    for x in xs:
        y = complicated_model(x, wait=False)
        ys.append(y)

    plt.scatter(xs, ys)
    plt.plot(xs, ys)
```

Let's see what happens when choosing `n`.


```python
sweep(n=30)
```

```python
sweep(n=50)
```

```python
sweep(n=100)
```

That's starting to look better, but let's increase the number of points such that we are sure we're not missing anything.

```python
sweep(n=200)
```

Seems like 200 is enough to capture the peak. But can we do better?


## The solution: Adaptive
![](https://adaptive.readthedocs.io/en/latest/_static/logo.png)

[![GitHub](https://img.shields.io/github/stars/python-adaptive/adaptive.svg?style=social)](https://github.com/python-adaptive/adaptive/)
[![documentation](https://readthedocs.org/projects/adaptive/badge/?version=latest)](https://adaptive.readthedocs.io/en/latest/?badge=latest)


* open-source Python library
* adaptive and parallel function evaluation made simple
* only need to supply a function and its bounds
* automatically parallelized
* live-plotting of the data
* written during my ([@basnijholt](https://github.com/basnijholt/)) Ph.D. to speed up large simulations of Majorana devices on the cluster

See https://adaptive.readthedocs.io for the documentation and https://github.com/python-adaptive/adaptive for the source code.

### Install using

```
pip install adaptive
# or
conda install adaptive
```


Let's calculate `complicated_model` using Adaptive!

```python
import adaptive
adaptive.notebook_extension()  # This is to enable the notebook integration
```

We create a "learner" object, which is a container for the function, bounds, and the data.

I will first show you how easy it is to run an adaptive simulation, and then we'll go into more details later.

```python
learner = adaptive.Learner1D(complicated_model, bounds=(-1, 1))
```

Now we need to tell this learner to do the things until a certain goal has been reached! 🦄


We can tell it to stop when it reached a certain number of points

```python
def points_goal(learner):
    return learner.npoints > 50
```

Or tell it to stop when the learner's "loss" has been reached.

```python
def loss_goal(learner):
    return learner.loss() < 0.05
```

Whatever this loss means, depends on what you choose it to mean. By default, for the `Learner1D`, it is the distance between neighboring points in `(x, y)`-space.

```python
runner = adaptive.Runner(learner, goal=loss_goal)
runner.live_info()
runner.live_plot(update_interval=0.5)
```

Note that the kernel is not blocked while the calculation is happening. So we can check out the currently known data for example:

```python
learner.data
```

We can compare the homogeneously sampled space to the adaptive learner like (_ignore the code_):

```python
%opts Layout [toolbar=None]

import itertools
import adaptive
from adaptive.learner.learner1D import uniform_loss, default_loss
import holoviews as hv
import numpy as np


def f(x, offset=0.07357338543088588):
    a = 0.01
    return x + a ** 2 / (a ** 2 + (x - offset) ** 2)


def plot_loss_interval(learner):
    if learner.npoints >= 2:
        x_0, x_1 = max(learner.losses, key=learner.losses.get)
        y_0, y_1 = learner.data[x_0], learner.data[x_1]
        x, y = [x_0, x_1], [y_0, y_1]
    else:
        x, y = [], []
    return hv.Scatter((x, y)).opts(style=dict(size=6, color="r"))


def plot(learner, npoints):
    adaptive.runner.simple(learner, lambda l: l.npoints == npoints)
    return (learner.plot() * plot_loss_interval(learner))[:, -1.1:1.1]


def get_hm(loss_per_interval, N=51):
    learner = adaptive.Learner1D(f, bounds=(-1, 1), loss_per_interval=loss_per_interval)
    plots = {n: plot(learner, n) for n in range(N)}
    return hv.HoloMap(plots, kdims=["npoints"])


(
    get_hm(uniform_loss).relabel("homogeneous samping")
    + get_hm(default_loss).relabel("with adaptive")
)
```

<!-- #region -->
## Let's break it down

### The learner

A learner takes the function to "learn" and the bounds.

```python
learner = adaptive.Learner1D(f, bounds=(-1, 1))
```

The three most important methods:

* `loss = learner.loss()` "quality factor" how well do the points describe the function?
* `xs, loss_improvements = learner.ask(n=10)` give me new points, and tell me how "good" the points are
* `learner.tell(x_new, y_new)` add the newly calculated data
<!-- #endregion -->

```python
learner.loss()
```

```python
xs, loss_improvements = learner.ask(n=5)
print(xs, loss_improvements)
```

```python
x = xs[0]
y = complicated_model(x)
learner.tell(x, y)  # should reduce the loss!

print(learner.loss())
```

<!-- #region -->
### The runner

We basically want to to something like _(not really how it works internally)_:

```python
learner = adaptive.Learner1D(f, bounds=(-1, 1))
while learner.loss() > 0.01:
    xs, loss_improvements = learner.ask(4)  # we do nothing with the `loss_improvements` now
    for x in xs:
        y = learner.function(x)
        learner.tell(x, y)
```
This has some problems:
* Not using all the resources (CPU cores)
* Blocks the kernel, so we cannot plot the data while the calculation is in progress

The Runner solves these problems.

```python
learner = adaptive.Learner1D(f, bounds=(-1, 1))
runner = adaptive.Runner(learner, goal=lambda l: l.loss() < 0.01)
```

<!-- #endregion -->

## Beyond one dimension

We have implemented different types of learners, some examples are:

* [`Learner1D`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner1D.html), for 1D functions f: ℝ → ℝ^N,

* [`Learner2D`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner2D.html), for 2D functions f: ℝ^2 → ℝ^N,

* [`LearnerND`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learnerND.html), for ND functions f: ℝ^N → ℝ^M,

* [`AverageLearner`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.average_learner.html), For stochastic functions where you want to average the result over many evaluations,

* [`IntegratorLearner`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.integrator_learner.html), for when you want to intergrate a 1D function f: ℝ → ℝ.




```python
def ring(xy, wait=True):
    import numpy as np
    from time import sleep
    from random import random

    if wait:
        sleep(random() / 10)
    x, y = xy
    a = 0.2
    return x + np.exp(-((x ** 2 + y ** 2 - 0.75 ** 2) ** 2) / a ** 4)


learner = adaptive.Learner2D(ring, bounds=[(-1, 1), (-1, 1)])
```

```python
runner = adaptive.Runner(learner, goal=lambda l: l.loss() < 0.01)
```

Besides just calling `learner.plot()`, one can customize the plotting function.

The plotting is using [HoloViews](http://holoviews.org/) (which I highly recommend to use in other research projects too.)

```python
def plot(learner):
    plot = learner.plot(tri_alpha=0.2)
    return (plot.Image + plot.EdgePaths.I + plot).cols(2)


runner.live_info()
runner.live_plot(plotter=plot, update_interval=0.1)
```

## Custom adaptive logic

<!-- #region -->
`Learner1D` and `Learner2D` both work on the principle of subdividing their domain into subdomains, and assigning a property to each subdomain, which we call the loss. The algorithm for choosing the best place to evaluate our function is then simply _take the subdomain with the largest loss and add a point in the center, creating new subdomains around this point._

The loss function that defines the loss per subdomain is the canonical place to define what regions of the domain are “interesting”. The default loss function for `Learner1D` and `Learner2D` is sufficient for a wide range of common cases, but it is by no means a panacea. For example, the default loss function will tend to get stuck on divergences.


One can specify the loss function like
<!-- #endregion -->

```python
def uniform_sampling_1d(xs, ys):
    dx = xs[1] - xs[0]
    return dx

def f_divergent_1d(x):
    if x == 0:
        return np.inf
    return 1 / x**2

learner = adaptive.Learner1D(f_divergent_1d, (-1, 1), loss_per_interval=uniform_sampling_1d)
runner = adaptive.Runner(learner, goal=lambda l: l.loss() < 0.01)
await runner.task  # wait until the runner is done
learner.plot().select(y=(0, 10000))
```

### Exercise 1.

You have a limited time but want the best resolution time can affort you.

Write a `time_goal` function to make the runner stop after 10 seconds.

```python
# Complete this cell

import time

def ring(xy, wait=True):
    import numpy as np
    from time import sleep
    from random import random

    if wait:
        sleep(random() / 10)
    x, y = xy
    a = 0.2
    return x + np.exp(-((x ** 2 + y ** 2 - 0.75 ** 2) ** 2) / a ** 4)


time_start = time.time()

def time_goal(learner):
    time_now = time.time()
    return .... # finsh this line


learner = adaptive.Learner2D(ring, bounds=[(-1, 1), (-1, 1)])
runner = adaptive.Runner(learner, goal=time_goal)
runner.live_info()
runner.live_plot(plotter=plot, update_interval=0.1)
```

```python
# Solution

# Complete this cell

import time

def ring(xy, wait=True):
    import numpy as np
    from time import sleep
    from random import random

    if wait:
        sleep(random() / 10)
    x, y = xy
    a = 0.2
    return x + np.exp(-((x ** 2 + y ** 2 - 0.75 ** 2) ** 2) / a ** 4)


time_start = time.time()

def time_goal(learner):
    time_now = time.time()
    return time_now - time_start > 10


learner = adaptive.Learner2D(ring, bounds=[(-1, 1), (-1, 1)])
runner = adaptive.Runner(learner, goal=time_goal)
runner.live_info()
runner.live_plot(plotter=plot, update_interval=0.1)
```

### Exercise 2.

Both the `Learner1D` and `Learner2D` allow you to specify a custom loss function. Below we illustrate how you would go about writing your own loss function. The documentation for `Learner1D` and `Learner2D` specifies the signature that your loss function needs to have in order for it to work with adaptive.

tl;dr, one can use the following loss functions that we already implemented:

* [`adaptive.learner.learner1D.default_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner1D.html#adaptive.learner.learner1D.default_loss)
* [`adaptive.learner.learner1D.uniform_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner1D.html#adaptive.learner.learner1D.uniform_loss)
* [`adaptive.learner.learner1D.curvature_loss_function`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner1D.html#adaptive.learner.learner1D.curvature_loss_function)
* [`adaptive.learner.learner1D.abs_min_log_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner1D.html#adaptive.learner.learner1D.abs_min_log_loss)
* [`adaptive.learner.learner2D.default_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner2D.html#adaptive.learner.learner2D.default_loss)
* [`adaptive.learner.learner2D.uniform_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner2D.html#adaptive.learner.learner2D.uniform_loss)
* [`adaptive.learner.learner2D.minimize_triangle_surface_loss`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner2D.html#adaptive.learner.learner2D.minimize_triangle_surface_loss)
* [`adaptive.learner.learner2D.resolution_loss_function`](https://adaptive.readthedocs.io/en/latest/reference/adaptive.learner.learner2D.html#adaptive.learner.learner2D.resolution_loss_function)

Try a different loss function for the `f_divergent_1d` function.

```python
# Complete this cell
from adaptive.learner.learner1D import curvature_loss_function

def f_divergent_1d(x):
    if x == 0:
        return np.inf
    return 1 / x**2

loss_per_interval = curvature_loss_function()
learner = adaptive.Learner1D(f_divergent_1d, (-1, 1))

runner = adaptive.Runner(learner, goal=lambda l: l.loss() < 0.01)
await runner.task  # wait until the runner is done
learner.plot().select(y=(0, 10000))
```

```python
# Solution
from adaptive.learner.learner1D import curvature_loss_function

def f_divergent_1d(x):
    if x == 0:
        return np.inf
    return 1 / x**2

loss_per_interval = curvature_loss_function()
learner = adaptive.Learner1D(f_divergent_1d, (-1, 1), loss_per_interval=loss_per_interval)

runner = adaptive.Runner(learner, goal=lambda l: l.loss() < 0.01)
await runner.task  # wait until the runner is done
learner.plot().select(y=(0, 10000))
```

### Exercise 3.

Use the `kwant` code that constructs a AB ring from the `1_introduction.md` lecture (also below) and calculate the `conductance` function adaptively.

<!-- #region -->
Convert the loop below to use Adaptive
```python
EF = 0.5
Bs = np.linspace(0.06, 0.1, 500)
gs = []
for B in Bs:
    gs.append(conductance(B))
plt.plot(Bs, gs, "k-")
plt.ylabel("$dI_3/dV_0 \quad [e^2/h]$")
plt.xlabel("$B$")
```
<!-- #endregion -->

```python
# Complete this cell
import kwant

def make_syst():

    lat = kwant.lattice.honeycomb()  # graphene lattice
    syst = kwant.Builder()  # An empty system
    r1, r2 = 5, 10

    def hopx(site1, site2, B):
        # The magnetic field is controlled by the parameter B
        y = site1.pos[1]
        return -np.exp(-1j * B * y)

    def TIE_fighter(pos):
        (x, y) = pos
        rr = x ** 2 / 8 + y ** 2
        ring = r1 ** 2 < rr < r2 ** 2
        wing = x < r1 // 2 and x > -r1 // 2 and (abs(y - r2) < r1 or abs(y + r2) < r1)
        upper = x < r2 and x > -r2 and y > r2 + r1 and y < r2 + 2 * r1
        lower = x < r2 and x > -r2 and y < -r2 - r1 and y > -r2 - 2 * r1
        return ring or wing or upper or lower

    syst[lat.shape(TIE_fighter, (0, r1 + 1))] = 0
    syst[lat.neighbors()] = -1

    #########################################################
    # New magic line:
    syst[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    #########################################################

    syst.eradicate_dangling()  # Quite convenient when using lat.shape()

    sym = kwant.TranslationalSymmetry(
        (-1, 0)
    )  # The lead will be invariant by translation

    # TODO : add leads
    lead_upper = kwant.Builder(sym)

    def upper(pos):
        (x, y) = pos
        return x < 20 and x > -20 and y > r2 - 1 and y < r2 + 2 * r1

    # Beware the second argument of lat.shape is in real space
    lead_upper[lat.shape(upper, lat.a(-8, 17).pos)] = 0

    lead_upper[lat.neighbors()] = -1
    lead_upper[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    syst.attach_lead(lead_upper)
    syst.attach_lead(lead_upper.reversed())

    lead_lower = kwant.Builder(sym)

    def lower(pos):
        (x, y) = pos
        return x < 20 and x > -20 and y < -r2 + 1 and y > -r2 - 2 * r1

    # Beware the second argument of lat.shape is in real space
    lead_lower[lat.shape(lower, lat.a(-8, -19).pos)] = 0
    lead_lower[lat.neighbors()] = -1
    lead_lower[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    lead_lower.eradicate_dangling()
    syst.attach_lead(lead_lower)
    syst.attach_lead(lead_lower.reversed())
    return syst.finalized()

fsyst = make_syst()

def conductance(B, fsyst=fsyst, EF=0.5):
    S = kwant.smatrix(fsyst, EF, params=dict(B=B))
    return S.transmission(3, 0)
```

```python
# Solution
import kwant

def make_syst():

    lat = kwant.lattice.honeycomb()  # graphene lattice
    syst = kwant.Builder()  # An empty system
    r1, r2 = 5, 10

    def hopx(site1, site2, B):
        # The magnetic field is controlled by the parameter B
        y = site1.pos[1]
        return -np.exp(-1j * B * y)

    def TIE_fighter(pos):
        (x, y) = pos
        rr = x ** 2 / 8 + y ** 2
        ring = r1 ** 2 < rr < r2 ** 2
        wing = x < r1 // 2 and x > -r1 // 2 and (abs(y - r2) < r1 or abs(y + r2) < r1)
        upper = x < r2 and x > -r2 and y > r2 + r1 and y < r2 + 2 * r1
        lower = x < r2 and x > -r2 and y < -r2 - r1 and y > -r2 - 2 * r1
        return ring or wing or upper or lower

    syst[lat.shape(TIE_fighter, (0, r1 + 1))] = 0
    syst[lat.neighbors()] = -1

    #########################################################
    # New magic line:
    syst[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    #########################################################

    syst.eradicate_dangling()  # Quite convenient when using lat.shape()

    sym = kwant.TranslationalSymmetry(
        (-1, 0)
    )  # The lead will be invariant by translation

    # TODO : add leads
    lead_upper = kwant.Builder(sym)

    def upper(pos):
        (x, y) = pos
        return x < 20 and x > -20 and y > r2 - 1 and y < r2 + 2 * r1

    # Beware the second argument of lat.shape is in real space
    lead_upper[lat.shape(upper, lat.a(-8, 17).pos)] = 0

    lead_upper[lat.neighbors()] = -1
    lead_upper[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    syst.attach_lead(lead_upper)
    syst.attach_lead(lead_upper.reversed())

    lead_lower = kwant.Builder(sym)

    def lower(pos):
        (x, y) = pos
        return x < 20 and x > -20 and y < -r2 + 1 and y > -r2 - 2 * r1

    # Beware the second argument of lat.shape is in real space
    lead_lower[lat.shape(lower, lat.a(-8, -19).pos)] = 0
    lead_lower[lat.neighbors()] = -1
    lead_lower[kwant.builder.HoppingKind((1, 0), lat.a, lat.b)] = hopx
    lead_lower.eradicate_dangling()
    syst.attach_lead(lead_lower)
    syst.attach_lead(lead_lower.reversed())
    return syst.finalized()

fsyst = make_syst()

def conductance(B, fsyst=fsyst, EF=0.5):
    S = kwant.smatrix(fsyst, EF, params=dict(B=B))
    return S.transmission(3, 0)


learner = adaptive.Learner1D(conductance, bounds=(0.06, 0.1))
runner = adaptive.Runner(learner, goal=lambda learner: learner.loss() < 0.01)
runner.live_info()
runner.live_plot(update_interval=0.1)
```
