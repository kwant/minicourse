---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.6.0
  kernelspec:
    display_name: Python [conda env:.conda-kwant_conda]
    language: python
    name: conda-env-.conda-kwant_conda-py
---

# Advanced topic: Symmetry analysis with Qsymm

**Symmetry finder and symmetric Hamiltonian generator**

`qsymm` is an open-source Python library that makes symmetry analysis simple.

It automatically generates model Hamiltonians from symmetry constraints
and finds the full symmetry group of your Hamiltonian.

Check out the [introductory tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/basics.html) to see examples of how to use `qsymm`.

## Implemented algorithms

![summary of methods](images/qsymm_summary.svg "Summary of methods")

The two core concepts in `qsymm` are _Hamiltonian families_ (Hamiltonians that may depend on
free parameters) and _symmetries_. We provide powerful classes to handle these:

+ `Model` is used to store symbolic Hamiltonians that may depend on momenta and other free parameters.
  We use `sympy` for symbolic manipulation, but our implementation utilizes `numpy` arrays for
  efficient calculations with matrix valued functions.

+ `PointGroupElement` and `ContinuousGroupGenerator` are used to store symmetry operators.
  Besides the ability to combine symmetries, they can also be applied to a `Model` to transform it.

We implement algorithms that form a two-way connection between Hamiltonian families and symmetries.

+ Symmetry finding is handled by `symmetries`, it takes a `Model` as input and finds all of its symmetries,
  including conserved quantities, time reversal, particle-hole, and spatial rotation symmetries.
  See the [symmetry finder tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/symmetry_finder.html) and the
  [kekule tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/kekule.html) for detailed examples.

+ `continuum_hamiltonian` and `bloch_family` are used to generate __k.p__ or lattice Hamiltonians
  from symmetry constraints. See the [k-dot-p generator tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/kdotp_generator.html),
  the [Bloch generator tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/symmetry_finder.html) and the
  [kekule tutorial](https://qsymm.readthedocs.io/en/latest/tutorial/kekule.html) for detailed examples.

We also provide an interface with `kwant` in `kwant.qsymm` allowing conversion between `qsymm.Model` and `kwant.Builder`.

## Installation
`qsymm` works with Python >= 3.5 and is available on PyPI and conda.

## Documentation
Qsymm's documentation is hosted on [Read the Docs](https://qsymm.readthedocs.io/en/latest/) and details of the algorithms can be found in [our paper](https://doi.org/10.1088/1367-2630/aadf67):

    Dániel Varjas, Tómas Ö Rosdahl, and Anton R Akhmerov
    Qsymm: algorithmic symmetry finding and symmetric Hamiltonian generation
    New J. Phys. 20 093026 (2018)

## Development
`qsymm` is on [Gitlab](https://gitlab.kwant-project.org/qt/qsymm), visit there if you would
like to to contribute, report issues, or get the latest development version.

# Analyze symmetries of graphene model from Kwant

```python extensions={"jupyter_dashboards": {"version": 1, "views": {"grid_default": {"hidden": true}, "report_default": {}}}}
import qsymm

import kwant
from kwant.qsymm import builder_to_model, model_to_builder, find_builder_symmetries

from collections import OrderedDict
import matplotlib.pyplot as plt

import sympy
sympy.init_printing(print_builtin=True)

import numpy as np
np.set_printoptions(precision=2, suppress=True)

print(kwant.__version__) # >= 1.4.2
print(qsymm.__version__) # >= 1.3.0
print(sympy.__version__) # NOT 1.5
```

Using Kwant, we construct the Hamiltonian for bulk graphene.

```python
lat = kwant.lattice.honeycomb(norbs=[1, 1])
sym = kwant.TranslationalSymmetry(lat.vec((1, 0)), lat.vec((0, 1)))

bulk = kwant.Builder(sym)
bulk[[lat.a(0, 0), lat.b(0, 0)]] = 0
bulk[lat.neighbors()] = lambda site1, site2, t: t
kwant.plot(bulk)
wrapped = kwant.wraparound.wraparound(bulk).finalized()
kwant.wraparound.plot_2d_bands(wrapped, params=dict(t=1));
```

To perform symmetry analysis on the Hamiltonian, we convert the unfinalized bulk model to the format that qsymm uses.

```python
ham = builder_to_model(bulk)
```

```python
type(ham)
```

```python
ham
```

To verify that the Hamiltonian looks as expected, qsymm allows for easy conversion to sympy.

```python
ham.tosympy(nsimplify=True)
```

The symmetry group is the full 2D hexagonal group combined with time-reversal and particle-hole symmetry.

```python
hex_group_2D = qsymm.groups.hexagonal()
```

```python
sg, cg = qsymm.symmetries(ham, hex_group_2D, prettify=True, generators=False)
print(len(sg))
print(set(sg) == qsymm.groups.hexagonal())
print(cg)
```

Look at only the generators of the group

```python
sg, cg = qsymm.symmetries(ham, hex_group_2D, prettify=True, generators=True)
print(len(sg))
sg
```

For instance, the Hamiltonian has the chiral symmetry $C = -\sigma_z$, and time-reversal symmetry $T = I \mathcal{K}$.

```python
C = [symmetry for symmetry in sg if symmetry == qsymm.groups.chiral(2)]
print(len(C))
C = C[0]
print(C)
```

```python
type(C)
```

```python
T = [symmetry for symmetry in sg if symmetry == qsymm.groups.time_reversal(2)]
print(len(T))
T = T[0]
print(T)
```

Adding an onsite term $\propto \sigma_z$ breaks the chiral symmetry.

```python
staggered_onsite = qsymm.BlochModel({qsymm.sympify('U'): np.array([[1, 0], [0, -1]])}, momenta=ham.momenta)
```

```python
ham_with_onsite = ham + staggered_onsite
```

```python
ham_with_onsite.tosympy(nsimplify=True)
```

Consequently, we no longer find a chiral symmetry of the Hamiltonian.

```python
sg, cg = qsymm.symmetries(ham_with_onsite, hex_group_2D, prettify=True, generators=True)
print(len(sg))
print(cg)
sg
```

```python
C = [symmetry for symmetry in sg if symmetry == qsymm.groups.chiral(2)]
```

```python
len(C) == 0
```

Finally, we convert the qsymm Hamiltonian back to Kwant. For this, we need to specify the number of sites (atoms) per unit cells along with their number of orbitals. In addition, we must provide the positions of the sites within a single unit cell, and the lattice vectors.

```python
norbs = OrderedDict({'A': 1, 'B': 1})  # A and B atom per unit cell, one orbital each.
# We extract the lattice vectors and the atom positions from the Kwant model above.
lat_vecs = [lat.vec((1, 0)), lat.vec((0, 1))]  # Bravais lattice vectors.
atom_coords = [site.pos for site in bulk.sites()]  # Atom coordinates
```

```python
bulk_from_model = model_to_builder(ham_with_onsite, norbs, lat_vecs, atom_coords)
```

With the onsite potential $U = 0$, we reproduce the graphene dispersion above.

```python
params = dict(U=0, t=1)
wrapped = kwant.wraparound.wraparound(bulk_from_model).finalized()
kwant.wraparound.plot_2d_bands(wrapped, params=params);
```

With the onsite potential $U ≠ 0$, we open a gap at the K points

```python
params = dict(U=1, t=1)
wrapped = kwant.wraparound.wraparound(bulk_from_model).finalized()
kwant.wraparound.plot_2d_bands(wrapped, params=params);
```

# Exercise


Analyze the symmetries of the continuum 1D Hamiltonian defined earlier for the Majorana wire. First we turn it into a `qsymm.Model`:

```python
majorana_hamiltonian = """
                       (k_x**2 / 2 - mu) * kron(sigma_0, sigma_z)
                        + alpha * k_x * kron(sigma_y, sigma_z)
                        + Delta * kron(sigma_0, sigma_x)
                        + B_x * kron(sigma_x, sigma_0)
                       """
ham = qsymm.Model(majorana_hamiltonian, momenta=['k_x'])
```

Find all the symmetries of this Hamiltonian!

+ Use the function `qsymm.symmetries`. By default it looks for onsite symmetries (time-reversal, particle-hole and chiral).
+ To find spatial symmetries as well (there is only one in 1D, what is it?) you will need to supply `candidates`. You can find pre-defined symmetry operators in `qsymm.groups`, such as `qsymm.groups.time_reversal(1)`.

You should see that there are several symmetries, but for the Majorana wire only particle-hole is essential. Find terms you can add to the Hamiltonian that break all other symmetries and only leave particle-hole.

+ Use the function `qsymm.continuum_hamiltonian` to find terms compatible with particle-hole.
+ Check that adding them to the original model, no symmetries are left but particle-hole.


## Solution


The default of `qsymm.symmetries` finds all onsite symmetries.

```python
sg, cg = qsymm.symmetries(ham)
sg
```

We already see that there is time-reversal and chiral symmetry. Let's check what TR looks like!

```python
T = [symmetry for symmetry in sg if symmetry == qsymm.groups.time_reversal(1)]
print(len(T))
T = T[0]
print(T)
```

It is a spinless TR symmetry that squares to +1, the magnetic field in the Hamiltonian already breaks fermionic TR.


Let's look for spatial symmetries as well, the only one in 1D is inversion (same as mirror). We see that it is also a symmetry.qsymm.groups.particle_hole

```python
candidates = [qsymm.groups.particle_hole(1),
              qsymm.groups.time_reversal(1),
              qsymm.groups.chiral(1),
              qsymm.groups.inversion(1)]
sg, cg = qsymm.symmetries(ham, candidates=candidates)
sg
```

Let's pick out PH symmetry.

```python
P = [symmetry for symmetry in sg if symmetry == qsymm.groups.particle_hole(1)]
print(len(P))
P = P[0]
print(P)
```

And generate a family of constant terms that obey PH. `qsymm.continuum_hamiltonian` returns a list of models, we turn it into a single model in the next line, then display it in latex format. The `c` are free real coefficients.

```python
family = qsymm.continuum_hamiltonian([P], 1, 0, prettify=True)
ham2 = qsymm.hamiltonian_generator.hamiltonian_from_family(family, tosympy=False)
ham2.tosympy(nsimplify=True)
```

We can simply add it to the first Hamiltonian.

```python
ham3 = ham + ham2
ham3.tosympy(nsimplify=True)
```

And finally check that only PH is left as intended

```python
sg, cg = qsymm.symmetries(ham3, candidates=candidates)
sg
```
